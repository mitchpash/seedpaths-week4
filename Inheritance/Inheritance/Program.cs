﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            var employees = new List<Employee>()
            {
                new Employee("Mitchell", "Pash", "Full Stack Developer"),
                new Employee("Keith", "Person", "Back End Developer"),
                new Employee("Umar", "Muqtadir", "Front End Developer")
            };
            foreach (var employee in employees)
            {
                employee.Walk();
                employee.Talk();
                Console.WriteLine();
            }

            var musicians = new List<Musician>()
            {
                new Musician("Bob", "Hope", new List<string>() {"Sax", "Keyboard", "Guitar"}),
                new Musician("David", "Bowie", new List<string>() {"Syntheses", "Piano", "Drums"})
            };
            foreach (var musician in musicians)
            {
                musician.Talk();
                Console.WriteLine();
                //Play some jams
                //musician.Jam();
            }
        }
    }

    #region Abstract Class
    /// <summary>
    /// Abstract class that defines a basic person
    /// </summary>
    public abstract class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public Person(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public void Walk()
        {
            Console.WriteLine("**whistling sounds**");
        }
        //virtual modifier: Override the behavior in a child class. This is only needed on a class marked as abstract.
        public virtual void Talk()
        {
            Console.WriteLine("Hello! My name is {0} {1}", FirstName, LastName);
        }
    }
    #endregion

    #region Inheritance Class

    public class Employee : Person
    {
        public string JobTitle { get; set; }

        public Employee(string firstName, string lastName, string jobTitle) : base(firstName, lastName)
        {
            JobTitle = jobTitle;
        }
        //overwrite the talk method
        public override void Talk()
        {
            base.Talk();
            Console.WriteLine("I am a {0}", JobTitle);
        }

        public override string ToString()
        {
            return string.Format("{0}, {1}, {2}", FirstName, LastName, JobTitle);
        }
    }
    #endregion
    /// <summary>
    /// Creates a new Musician with a job title of Musician
    /// </summary>
    public class Musician : Employee
    {
        public List<string> PlayableInstruments { get; set; }
        public Musician(string firstName, string lastName, List<string> playableInstruments) : base(firstName, lastName, "Musician")
        {
            PlayableInstruments = playableInstruments;
        }
        /// <summary>
        /// Greet and list playable instruments
        /// </summary>
        public override void Talk()
        {
            base.Talk();
            Console.WriteLine("I like to play :");
            foreach (var playableInstrument in PlayableInstruments)
            {
                Console.WriteLine("{0} ", playableInstrument);
            }
        }
        /// <summary>
        /// Jam out on the console to Dot Net Perls' official score
        /// </summary>
        public void Jam()
        {
            // The official music of Dot Net Perls.
            for (int i = 37; i <= 32767; i += 4000)
            {
                Console.Beep(i, 100);
            }
        }
    }

    public class PooperScooper : Employee
    {
        public enum Scooper
        {
            Hand,
            Shovel,
            PowerPooperScooper
        }

        public int PoopsScooped { get; set; }
        public Scooper TypeOfScooper { get; set; }
        public PooperScooper(string firstName, string lastName, Scooper scooper) : base(firstName, lastName, "Pooper Scooper")
        {
            TypeOfScooper = scooper;
        }

        public void ScoopPoop(int i)
        {
            PoopsScooped += i;
        }
    }
}
