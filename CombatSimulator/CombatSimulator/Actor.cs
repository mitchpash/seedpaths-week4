﻿using System;

namespace CombatSimulator
{
    public abstract class Actor
    {
        public readonly Random Rng = new Random(); //Random number generator
        public string Name { get; set; } //Name for display purposes
        public int HP { get; set; } //HP to compare health
        public int MaxHP { get; set; } //Maximum amount of health a player is given
        public int MinDamage { get; set; } //Minimum amount of damage an actor can cause (20 deafult)
        public int MaxDamage { get; set; } //Maximum amount of damage an actor can cause (35 default)

        /// <summary>
        /// Creates a new actor with a name, health points, min damage (20 deafult), and max damage (35 deafult)
        /// </summary>
        /// <param name="name"></param>
        /// <param name="hp"></param>
        /// <param name="minDamage"></param>
        /// <param name="maxDamage"></param>
        protected Actor(string name, int hp, int minDamage = 20, int maxDamage = 35)
        {
            Name = name;
            HP = hp;
            MaxHP = hp;
            MinDamage = minDamage;
            MaxDamage = maxDamage;
        }

        /// <summary>
        /// Implement damaging another actor's HP
        /// </summary>
        /// <param name="actor"></param>
        public virtual void Attack(Actor actor)
        {
        }

        /// <summary>
        /// Check to see if the actor is alive
        /// </summary>
        public bool IsAlive => HP > 0;

        /// <summary>
        /// Returns if a Player is able to hit based on their chanceToHit. 1 = 10%, 10 = 100%
        /// </summary>
        /// <param name="chanceToHit"></param>
        /// <returns></returns>
        public bool CanHit(int chanceToHit) => Rng.Next(0, 101) <= chanceToHit;

    }
}