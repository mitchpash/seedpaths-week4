﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CombatSimulator
{
    class Game
    {
        private const int Delay = 1000; //Delay between actions or 'abilities'

        static Player Player { get; set; }
        static Enemy Enemy { get; set; }

        static void Main(string[] args)
        {
            GameGui gameGui = new GameGui();
            
            //Ask for user's name
            Console.Write("What is your name?: ");
            var playerName = Console.ReadLine();

            Player = new Player(playerName, 100, 20, 35);
            Enemy = new Enemy("Dragon", 200, 10, 15);

            gameGui.DrawIntro(); //Welcome the user (Splash Screen)

            while (Player.IsAlive && Enemy.IsAlive)
            {
                //Create new Game UI every action or 'ability'
                Console.Clear();
                gameGui.DrawGui(Player, Enemy); 

                //Ask the user what they would like to do
                Console.SetCursorPosition(10, 20);
                Console.WriteLine("What do you do?: ");
                Console.SetCursorPosition(10, 22);
                Console.WriteLine("1. Attack");
                Console.SetCursorPosition(10, 23);
                Console.WriteLine("2. Use Ability");
                Console.SetCursorPosition(10, 24);
                Console.WriteLine("3. Heal");

                int input;
                Console.SetCursorPosition(27, 20);
                if (int.TryParse(Console.ReadLine(), out input)) //Get user input and TryParse to int
                {
                    Console.Clear();
                    gameGui.DrawGui(Player, Enemy);

                    switch (input)
                    {
                        case 1: //Attack
                            Player.Attack(Enemy);
                            Thread.Sleep(Delay);
                            break;

                        case 2: //Cast Spell
                            Player.CastAbility(Enemy);
                            Thread.Sleep(Delay);
                            break;

                        case 3: //Heal
                            Player.CastHeal();
                            Thread.Sleep(Delay);
                            break;

                        default:
                            Console.SetCursorPosition(50, 22);
                            Console.Write("You don't know that skill yet...");
                            for (int i = 0; i < 3; i++) { Console.Write("."); Thread.Sleep(500); }
                            Console.WriteLine();

                            Thread.Sleep(Delay);
                            break;
                    }
                    Console.Clear();
                    gameGui.DrawGui(Player, Enemy);
                    
                    Enemy.Attack(Player);
                    Thread.Sleep(Delay);
                }
                else //Invalid input
                {
                    Console.SetCursorPosition(50, 22);
                    Console.WriteLine("That wasn't a valid choice...");
                    Thread.Sleep(Delay);
                }
            }

            Console.Clear();
            gameGui.DrawEnding();

            if (Player.IsAlive || Enemy.IsAlive)
            {
                if (!Enemy.IsAlive && Player.IsAlive) // Won the game
                {
                    Console.SetCursorPosition(35, 47);
                    Console.WriteLine("You have slain the dragon!");
                }
                else if (Enemy.IsAlive && !Player.IsAlive) //Lost the game
                {
                    Console.SetCursorPosition(35, 47);
                    Console.WriteLine("You have been slain by the {0}!", Enemy.Name);
                }
            }
            else //Tied the game
            {
                Console.SetCursorPosition(35, 47);
                Console.WriteLine("You were slain by each other in battle!");
            }

            //End of Program
            Console.ReadKey();
        }
    }
}