﻿using System;
using System.Threading;

namespace CombatSimulator
{
    class Enemy : Actor
    {
        /// <summary>
        /// Creates a new enemy to fight in the comming battle
        /// </summary>
        /// <param name="name"></param>
        /// <param name="hp"></param>
        /// <param name="minDamage"></param>
        /// <param name="maxDamage"></param>
        public Enemy(string name, int hp, int minDamage, int maxDamage) : base(name, hp, minDamage, maxDamage)
        {
        }
        /// <summary>
        /// Attacks an actor for 95% of their calculated attack damage.
        /// </summary>
        /// <param name="actor"></param>
        public override void Attack(Actor actor)
        {
            Console.SetCursorPosition(50, 22);
            Console.Write("The {0} lashes out with heavy flame", Name);
            for (int i = 0; i < 3; i++) { Console.Write("."); Thread.Sleep(500); }
            Console.WriteLine();

            if (!CanHit(80)) //Didn't hit the actor
            {
                Console.SetCursorPosition(50, 24);
                Console.WriteLine("You got out of the way just in time to miss the flame!");
            }
            else //Hits the actor
            {
                var attackValue = (int)Math.Floor(Rng.Next(MinDamage, MaxDamage + 1) * .95);
                Console.SetCursorPosition(50, 24);
                Console.WriteLine("The {1} burns your for {0} damage!", attackValue, Name);
                
                //All enemies hit for 95% of attackValue
                actor.HP -= attackValue;
            }
        }
    }
}