﻿using System;
using System.Threading;

namespace CombatSimulator
{
    class Player : Actor
    {
        /// <summary>
        /// Creates a new player that can fight with three abilities
        /// </summary>
        /// <param name="name"></param>
        /// <param name="hp"></param>
        /// <param name="minDamage"></param>
        /// <param name="maxDamage"></param>
        public Player(string name, int hp, int minDamage, int maxDamage) : base(name, hp, minDamage, maxDamage)
        {
        }
        /// <summary>
        /// Attacks an actor for 100% of their calculated attack damage.
        /// </summary>
        /// <param name="actor"></param>
        public override void Attack(Actor actor)
        {
            Console.SetCursorPosition(50, 22);
            Console.Write("You swing your sword with great strength");
            for (int i = 0; i < 3; i++) { Console.Write("."); Thread.Sleep(500); }
            Console.WriteLine();

            if (!CanHit(70)) // Can't hit
            {
                Console.SetCursorPosition(50, 24);
                Console.WriteLine("Your sword narrowly misses! Oh no!");
            }
            else // Can hit
            {
                var meleeAttack = Rng.Next(MinDamage, MaxDamage + 1);
                Console.SetCursorPosition(50, 24);
                Console.WriteLine("Your sword lands and hits the fierce dragon for {0} damage!", meleeAttack);
                actor.HP -= meleeAttack; //All melee attacks hit for 100% base attack damage
            }
        }

        /// <summary>
        /// Casts an ability dealing damage based on their 'abilityPower' (MinDamage, MaxDamage - 10)
        /// </summary>
        /// <returns></returns>
        public void CastAbility(Actor actor)
        {
            Console.SetCursorPosition(50, 22);
            Console.Write("You cast an ability with great skill");
            for (int i = 0; i < 3; i++) { Console.Write("."); Thread.Sleep(500); }
            Console.WriteLine();

            var abilityAttack = Rng.Next(MinDamage - 10, MaxDamage - 19);
            Console.SetCursorPosition(50, 24);
            Console.WriteLine("Your Bastion of Light hits the dragon for {0} damage!", abilityAttack);
            actor.HP -= abilityAttack;
        }

        /// <summary>
        /// Heals the player for 10-15 points
        /// </summary>
        /// <returns></returns>
        public void CastHeal()
        {
            Console.SetCursorPosition(50, 22);
            Console.Write("You call upon the Gods to grant you strength in your time of need!");
            for (int i = 0; i < 3; i++) { Console.Write("."); Thread.Sleep(500); }
            Console.WriteLine();

            var healAmmount = Rng.Next(10, 16);
            if (HP < MaxHP)
            {
                if (HP + healAmmount >= MaxHP) //The heal will bring the Player to MaxHP
                {
                    Console.SetCursorPosition(50, 24);
                    Console.WriteLine("You heal to max heath!");
                    HP = MaxHP;
                }
                else //Heal the player as normal
                {
                    Console.SetCursorPosition(50, 24);
                    Console.WriteLine("The gods grant you {0} health back!", healAmmount);
                    HP += healAmmount;
                }
            }
            else //Warn the user they are already at max health
            {
                //Punish for not looking at current health. Enemy WILL attack after.
                Console.SetCursorPosition(50, 24);
                Console.WriteLine("You're already at max health!");
            }
            
        }
    }
}